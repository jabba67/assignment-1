using System.Collections;
using UnityEngine;

public class Mover : MonoBehaviour {

	//private Rigidbody rb;
	public float speed;

	void start()
	{
		//rb = GetComponent<Rigidbody> ();
		//rb.velocity = transform.forward * speed;
		GetComponent<Rigidbody>().velocity = transform.forward * speed;

	}

}
